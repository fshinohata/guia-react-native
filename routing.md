Criando aplicações com múltiplas páginas
========================================

Originalmente, o `ReactJS` foi uma ferramenta utilizada apenas para fazer aplicações *single-page*. Mas as pessoas logo viram o potencial da *framework* e inventaram *workarounds* para simular uma aplicação multi-página.

Em `ReactJS`, o pacote mais utilizado para simular esse comportamento é o [`react-router` e o `react-router-dom`](https://github.com/ReactTraining/react-router).

O que vamos utilizar aqui é o pacote [`react-router-native`](https://github.com/ReactTraining/react-router/tree/master/packages/react-router-native), que funciona da mesma forma que o `react-router` e o `react-router-dom`. Ou seja, se você aprender a usar este, você também estará aprendendo o funcionamento da versão web (ReactJS).

Instalação
==========

O primeiro passo é instalar o pacote:

```bash
npm install --save react-router-native
```

Reinicie seu servidor react native se você o deixou ligado durante a instalação do pacote, e pronto.

Setup inicial
=============

Se sua aplicação não tem páginas ainda, vamos precisar criar algumas para testar. Mas antes disso, vamos definir nossa estrutura de pastas, para que você não se perca com as explicações.

Nos exemplos a seguir, estarei utilizando a seguinte estrutura de pastas:

![Estrutura de pastas do projeto teste](img/react-native-test-project-folder-structure.png "Estrutura de Pastas")

Dentro da pasta do projeto, junto ao arquivo `App.js`, eu tenho uma pasta chamada "ui" (acrônimo para *User Interface*), onde pretendo colocar tudo que é destinado à interface do usuário.

A pasta `ui/components/` contém os componentes "genéricos", que podem ser reutilizados em outros lugares.

A pasta `ui/layout/` contém o componente que será o *Layout* das páginas da aplicação.

A pasta `ui/screens/` contém pastas com as telas (ou páginas) da aplicação.

Veja o [projeto exemplo](https://gitlab.com/fshinohata/guia-react-native/tree/projeto-exemplo) se preferir.

Se você estiver copiando o que estou fazendo, aqui estão os componentes que serão utilizados:

```jsx
/* ui/screens/home/Home.js */
import React, { Component } from "react";
import { View, Text } from "react-native";

class Home extends Component {
    render() {
        return (
            <View>
                <Text>Home</Text>
            </View>
        );
    }
}

export default Home;
```

```jsx
/* ui/screens/about/About.js */
import React, { Component } from "react";
import { View, Text } from "react-native";

class About extends Component {
    render() {
        return (
            <View>
                <Text>Sobre</Text>
            </View>
        );
    }
}

export default About;
```

```jsx
/* ui/screens/errors/PageNotFound.js */
import React, { Component } from "react";
import { View, Text } from "react-native";
import { withRouter } from "react-router-native";

class PageNotFound extends Component {
    render() {
        return (
            <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Text>A rota "{this.props.location.pathname}" não existe</Text>
            </View>
        );
    }
}

export default withRouter(PageNotFound);
```

```jsx
/* ui/layout/LayoutStyles.js */
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
    },
    header: {
        width: "100%",
        flex: 2,
        borderBottomWidth: 1,
        borderBottomColor: "#000",
        justifyContent: "center",
        alignItems: "center",
    },
    main: {
        flex: 8,
        justifyContent: "center",
    }
});

export default styles;
```

```jsx
/* ui/layout/Layout.js */
import React, { Component } from "react";
import { View, Text } from "react-native";
import { Link } from "react-router-native";

import styles from "./LayoutStyles";

class Layout extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Link to="/"><Text>Ir para "Home"</Text></Link>
                    <Link to="about"><Text>Ir para "Sobre"</Text></Link>
                    <Link to="potato"><Text>Rota inválida</Text></Link>
                </View>
                <View style={styles.main}>
                { this.props.children }
                </View>
            </View>
        );
    }
}

export default Layout;
```

Repare que, no `Layout.js`, utilizamos um componente do `react-router-native` chamado `Link`, em três instâncias: Um que vai para a rota "/" (que deve renderizar o componente `Home.js`), outro para "about" (que deve renderizar o componente `About.js`), e outro para "potato" (que faremos com que seja uma rota inválida). Essas rotas não existem ainda.

Outro item interessante é o `export default withRouter(PageNotFound)`, no componente `PageNotFound.js`. Como você deve imaginar, ele só imprime que "a rota XYZ não existe". Esse comportamento é ótimo para detectar um `Link` criado com a rota errada.

A função `withRouter()` vai conectar o componente passado como argumento ao `<NativeRouter>`, resultando na criação das *props* `location`, `match`, e `history`, acessíveis via `this.props`. A *prop* `location` contém dados sobre a rota atual:

![Impressão das props do componente "PageNotFound" no Reactotron](img/react-native-test-project-withRouter.png "Impressão das props do componente \"PageNotFound\" no Reactotron")

Lembre-se que você sempre pode imprimir variáveis no Reactotron com `console.tron.log()` para depurar.

Para criar as rotas, os projetos React geralmente fazem isso no `App.js`, que é o ponto de entrada da aplicação. O meu ficou assim:

```jsx
/* App.js */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import { NativeRouter, Route, Switch } from "react-router-native";

import Layout from "./ui/layout/Layout";
import Home from "./ui/screens/home/Home";
import About from './ui/screens/about/About';
import PageNotFound from "./ui/screens/errors/PageNotFound";

export default class App extends Component {
    render() {
        return (
            <NativeRouter>
                <Layout>
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/about" component={About} />
                        <Route render={() => <PageNotFound />} />
                    </Switch>
                </Layout>
            </NativeRouter>
        );
    }
}

```

Vamos analisar os componentes utilizados no método `render()` por partes:

* O componente `<NativeRouter>` é o responsável por "ligar" o módulo de roteamento da aplicação;

* O componente `<Route>` estabelece uma "rota", cujo caminho é dado pela *prop* `path` e o componente a ser renderizado na rota é dado pela *prop* `component`. A *prop* `exact` é passada na rota com caminho "/" porque, caso contrário, se o usuário pedir a rota "/about", o componente a ser renderizado será a `Home`, pois sem a *prop* `exact` basta que o caminho esteja contido na rota (ou seja, "/" está dentro de "/about", então a `Home` será renderizada porque ela vem primeiro);

* O componente `<Switch>` é responsável por detectar mudanças na rota e renderizar a `<Route>` que bate com a rota pedida. Se nenhuma bater, o último `<Route>` (que não tem um `path`) é renderizado;

* O componente `<Layout>` fica sobre as rotas e o *switch*, pois ele será imutável na nossa aplicação.

Com isso, a aplicação resultante será esta:

Home:

<img src="img/react-native-test-project-home-screen.jpg" width="480" height= "720" alt="Tela 'Home'" title="Tela 'Home'"/>

Sobre:

<img src="img/react-native-test-project-about-screen.jpg" width="480" height= "720" alt="Tela 'Sobre'" title="Tela 'Sobre'"/>

Rota Inválida:

<img src="img/react-native-test-project-page-not-found-screen.jpg" width="480" height= "720" alt="Tela 'Rota Inválida'" title="Tela 'Rota Inválida'"/>

Com isso, temos nossa estrutura de roteamento pronta!

Só falta integrá-la com o *Redux* para sua aplicação ser escalável e bem comportada. :)
