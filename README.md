Ordem de leitura recomendada
============================

1. machine-setup.md

2. development-setup-and-tips.md

3. react-native.md

4. reactotron.md

5. routing.md

6. redux.md

7. api-calls.md

8. database.md (WIP)